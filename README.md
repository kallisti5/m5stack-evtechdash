# EV Tech Dashboard

This is an experimental firmware for the M5Stack to monitor your EV via a Bluetooth OBD2 scan tool.

Cars like the Chevy Bolt EV hide critical data from you. This small dashboard gives you insights into
your vehicle without the need to stare at your phone or setup custom PIDs

## Key Metrics

* Battery Temperature
* Battery State of Charge
* DC Current
* Individual Cell Voltages

## Requirements

* Linux Desktop
* M5Stack
* Platform I/O
* Bluetooth OBD2 Adapter (Switched)

## Target Vehicles

* Chevy Bolt EV

## Building / Uploading

```pio run -e m5stack-core-esp32 -t upload```
